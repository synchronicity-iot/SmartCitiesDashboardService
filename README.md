# SmartCities dashboard

## Description
Smart Cities Dashboard is a web service interfaces that provides end-users with a simple and user-friendly dashboard 
allowing a map-based representation of the devices connected. Currently the application is able to represent location, 
real-time sensor information. The current service version is relying on Google Maps API to create a web service that 
draws a map of the devices deployed along the world.

The challenges addressed by the tool are:
- Process data from multiple sources/sensors in real-time from SynchroniCity framework
- View flexibility to certain data sets (e.g. through dashboard widgets)
- Map as the main way of interaction to superimpose data sets
- The application must provide users with a geo-scoped search of data.
- User Interface is intuitive and easy to use

![alt text](images/smartcities-dashboard.png "SmartCitiesDashboard")

## How to use it
As shown in the previous figure the application provides a simple graphical interface allowing the users to visualize 
real-time information in the devices and entities instantiated in the Synchronicity framework context management API.

More information about how to integrate the data from the Synchronicity framework context management API within the application
can be found in the PDF file dashboard-integration_v2.pdf in the main root of the repository.

## Interface and API
The application provides a web-browser graphical interface. No API is required to use the tool. Only configuration process requires
to interact with the Synchronicity framework context management API. This process is described the PDF file dashboard-integration_v2.pdf in the main root of the repository.

## Service Architecture
The following figure represents a service architecture overview of the proposed scenario.

![Service Architecture](images/figure1.png "Service Architecture")

1. *Synchronicity framework: the solution uses the data coming from the context management API:*
2. *SmartCities Backend:* this service acts as a bridge requesting and adapting the information provided by the context management API and sending it to the frontend.
3. *SmartCities Frontend:* this service provides a graphical interface to access and visualize the different devices. Map and chart representations are provided.

## Service Deployment
```yaml
version: '3.3'

services:
 synchronicity-backend:
    image: synchronicityiot/dashboard-backend:1.2
    environment:
      - ORION_URL=http://sync-orion:1026
      - ORION_SERVICE=smartspot
      - ORION_SERVICEPATH=/map/hopu/#
      - DJANGO_DEBUG=True
      - DJANGO_ALLOWED_HOSTS="*"
    networks:
     - dashboard-net

 synchronicity-frontend:
   image: synchronicityiot/dashboard-frontend:1.5.1
   ports:
     - "8080:80"
   environment:
     - SITE_URL=192.168.1.192
     - EXTERNAL_URL=192.168.1.192:8080
     - GOOGLE_MAPS_API=AIzaSyCwdzyyqxxk1-toz8XIti7oy8cSWIVqCtI
     - SYNCHRONICITY_BACKEND=synchronicity-backend:8000
   networks:
     - dashboard-net

 mongo:
   image: mongo:3.2
   command: --nojournal
   networks:
     - dashboard-net

 sync-orion:
   image: fiware/orion:2.2.0
   ports:
     - "1026:1026"
   command: '-dbhost mongo -logForHumans -logLevel DEBUG'
   networks:
     - dashboard-net

networks:
  dashboard-net:
    external:
      name: synchronicityNetwork

```

The configuration is explained below:
*Frontend configuration*
Three environmental variables have to been taking into account in order create custom configurations of the component:

- **SITE_URL →** allows dockerized nginx to redirect the incoming traffic base on the domain name
- **GOOGLE_MAPS_API →** allows the user to consume Google Maps API. This environmental variable is required
- **SYNCHRONICITY_BACKEND →** backend url.
- **EXTERNAL_URL →** set the request backend url in the frontend http client

*Backend configuration*
Four environmental variables have to been taking into account in order create custom configurations of the component:

- **ORION_URL →** Sets the url pointing the Orion instance out.
- **ORION_SERVICE →** Sets the service header in Orion requests.
- **ORION_SERVICEPATH →** Sets the service-path header in Orion requests
- **DJANGO_DEBUG=False →** Enable or disable the django debug flag.

### Build architecture through docker-compose

Launch Atomic Service:

```
$ docker-compose up
```

Or launch service in background:
```
$ docker-compose up -d
```

Stop service:
```
$ docker-compose down
```

### Check container running

A simple way to test the atomic service is running on docker, is using the next command:
```
$ docker ps
```
Now appear four running services along with its attributes, this table contains ports, names, images of the docker containers. To obtain the data in a more compact form use:
```
$ docker ps --format 'Service: {{.Names}} \nPorts: {{.Ports}}\n'
```

## System requirements
Minimum requirements to run this atomic service:
- docker
- docker-compose

## Support
Once the dashboard has been deployed, it is important to keep updated, without mistakes and safe to hackers’ attacks. There is no point in having an outdated dashboard, with safe mistakes, outdated content or with fault due to the fact that changes in technologies used as for example a bad visualization with the last versions in web browsers (Internet explorer, Firefox, Chrome, …) or others electronic devices such as smart phone or tablet.

For that reason, HOPU team offers a service support to Smartcities Dashboard which is detailed below:

- Changes in web content: texts, images, etc.
- Data loading in databases
- Code changes in dynamic web to changes the operation
- Incident diagnosis
- Performing checks
- Solving System incidents
- Solving incident related with the web code
- Solving functional mistakes
- Technical support
- Consultations about operation, use and exploitation of Smartcities dashboard

Which not include

- The development of new modules which add new functionalities to the Smartcities dashboard
- Redesign of web structure or the graphic appearance
- Incidents which have been caused by users in the original programs
- Computer equipment user´s setting

Service conditions

- The work schedules will be from Monday to Friday from 9 am to 7 pm. There will not be support service in public holidays.

To any technical issue, write your incidence in our issue tracker that you can find in the following link: [click me!](https://gitlab.com/synchronicity-iot/SmartCitiesDashboardService/issues)

## License & Terms and Conditions
This atomic service is a fully HOPU development and it has a private license. However, it will be free for partner and reference zones involved in SynchroniCity project through the main consortium or Open call.

Any organization which is interested in this service for other project outside SynchroniCity, in order to use it as part of their daily activity in some project or to be deployed in any external city, these organizations should contact with HOPU direction in order to define conditions in terms of fees and services included.

In the following link, you can read our Terms and Conditions about this service [click me!](https://github.com/HOP-Ubiquitous/live-dashboard/blob/master/LICENSE)