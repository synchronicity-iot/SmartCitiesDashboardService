import requests
import random
import json

def generar_cuerpo_mensaje(iteracion):
	tipo=["Vehicle","NoiseLevelObserved","PointOfInterest"]
	randomTipo = random.choice(tipo)
	numero_aleatorio_latitude = round(random.uniform(36,43),6)
	numero_aleatorio_longitude = round(random.uniform(-10,-0),6)
	mensaje={
		"contextElements": [
		    {
		        "type": randomTipo,
		        "isPattern": "false",
		        "id": "mapa" + str(iteracion),
		        "attributes": [
					{
		                "name": "dateModified",
		                "type": "text",
		                "value": "2018-07-06T13:09:14+00:00"
		            },
					{
		                "name": "location",
		                "type": "geo:json",
		                "value": {
		            			"type": "Point",
		            			"coordinates": [ float(numero_aleatorio_longitude), float(numero_aleatorio_latitude) ]
		        		}
		            },
		            {
		                "name": "latitude",
		                "type": "Number",
		                "value": numero_aleatorio_latitude
		            },
		            {
		                "name": "longitude",
		                "type": "Number",
		                "value": numero_aleatorio_longitude
		            },
		            {
		                "name": "speed",
		                "type": "Number",
		                "value": 1
		            },
		            {
		                "name": "vehicleType",
		                "type": "Text",
		                "value": "car"
		            }
		        ]
		    }
		],
		"updateAction": "APPEND"
	}
	return mensaje


if __name__ == "__main__":
	iteracion = 0
	while iteracion < 1000:
		mensaje = generar_cuerpo_mensaje(iteracion)
		url = "http://192.168.1.192:1026/v1/updateContext"
		headers = {
			'fiware-service': "smartspot",
			'fiware-servicepath': "/map/hopu/test",
			'Content-Type': "application/json",
			}

		response = requests.request("POST", url, headers=headers, data=json.dumps(mensaje))
		print(iteracion)
		iteracion = iteracion + 1

	print("FIN")
